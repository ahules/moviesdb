(function ($) {
  $('#rating-btn').on('click', function (evt) {
    evt.preventDefault();
    var rating = parseFloat($(this).prev('input').val());
    if (rating <= 10 && rating >= 1) {
      console.log(111);
      $.post('/movies/rate', {
        id: $(this).data('external-id'),
        rating: rating
      }).done(function () {
        alert('Success');
      }).fail(function () {
        error();
      })
    } else {
      error();
    }
  });

  function error() {
    alert('Wrong rating.')
  }

})(jQuery);