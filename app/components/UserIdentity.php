<?php


class UserIdentity extends CBaseUserIdentity implements IUserIdentity
{
    const ERROR_NONE = 0;
    const ERROR_DATA = -1;

    public $errorCode;

    protected $api_key;
    protected $expire;
    protected $session;

    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }

    public function authenticate()
    {
        /**
         * @var $api MovieDbClient
         */
        $api = Yii::app()->getComponent('movieApi')->client();
        $api->setApiKey($this->api_key);
        $result = $api->guestSession();

        if (!empty($result->guest_session_id) && !empty($result->success)) {
            $this->errorCode = self::ERROR_NONE;
            $this->expire = \DateTime::createFromFormat('Y-m-d H:i:s T', $result->expires_at)->getTimestamp();
            $this->session = $result->guest_session_id;
        } else if (is_object($result)) {
            $this->errorCode = $result->status_code;
            $this->errorMessage = $result->status_message;
        } else {
            $this->errorCode = self::ERROR_DATA;
            $this->errorMessage = 'API server return incorrect data.';
        }

        return !$this->errorCode;
    }

    /**
     * @inheritdoc
     */
    public function getIsAuthenticated()
    {
        return $this->errorCode == self::ERROR_NONE;
    }

    public function getExpire()
    {
        return $this->expire - time();
    }

    public function getSession()
    {
        return $this->session;
    }
}