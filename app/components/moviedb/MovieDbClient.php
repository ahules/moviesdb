<?php


/**
 * Class MovieDbClient
 * Partial Representation of API V3 of themoviedb.
 * All docs see here http://docs.themoviedb.apiary.io
 */

class MovieDbClient
{
    /**
     * Define API resources URL
     */
    const RES_BASE_URL = 'https://api.themoviedb.org/3';
    const RES_GUEST_SESSION = '/authentication/guest_session/new';
    const RES_MOVIE_POPULAR = '/movie/popular';
    const RES_DISCOVER_MOVIE = '/discover/movie';
    const RES_MOVIE = '/movie/:id';
    const RES_MOVIE_RATING = '/movie/:id/rating';
    const RES_IMG = 'http://image.tmdb.org/t/p/w:width/:img';

    protected $curl;
    protected $apiKey;
    protected $errorHandler;
    protected $lang;
    protected $session;

    /**
     * MovieDbClient constructor.
     */
    public function __construct($apiKey = null, $session = null,  $lang = 'en')
    {
        $this->curl = new Curl\Curl();
        $this->lang = $lang;
        $this->setSession($session);
        $this->setApiKey($apiKey);
    }

    public function setApiKey($apiKey)
    {
        if (empty($apiKey)) {
            return false;
        }

        $this->apiKey = $apiKey;

        return true;
    }

    public function setSession($session)
    {
        if (empty($session)) {
            return false;
        }

        $this->session = $session;

        return true;
    }

    public function setErrorHandler($callback)
    {
        if (!is_callable($callback)) {
            return false;
        }

        $this->errorHandler = $callback;

        return true;
    }

    public function getImageUrl($size, $file)
    {
        return strtr(self::RES_IMG, array(
            ':width' => $size,
            ':img' => $file
        ));
    }

    /**
     * @param array $config
     * @param int $page
     * @return mixed
     *
     * All supported config params please see there http://docs.themoviedb.apiary.io/#reference/discover/discovermovie
     * Example: $api->search(array('sort_by' => 'vote_average.asc', 'with_genres' => 1))
     */
    public function search($config = array(), $page = 1)
    {
        $config['page'] = $page;
        $result = $this->curl->get($this->makeUrl(self::RES_DISCOVER_MOVIE), $this->getParams($config));
        return $this->process($result);
    }

    public function getPopular($page = 1)
    {
        $result = $this->curl->get($this->makeUrl(self::RES_MOVIE_POPULAR), $this->getParams(array('page' => $page)));
        return $this->process($result);
    }

    public function getMovie($id)
    {
        $result = $this->curl->get(
            $this->makeUrl(self::RES_MOVIE, array(':id' => $id)),
            $this->getParams(array('append_to_response' => 'trailers'))
        );
        return $this->process($result);
    }

    public function guestSession()
    {
        $result = $this->curl->get($this->makeUrl(self::RES_GUEST_SESSION), $this->getParams());
        return $result;
    }

    public function pushMovieRating($id, $rating)
    {
        $url = $this->makeUrl(self::RES_MOVIE_RATING, array(':id' => $id));
        $curl = new \Curl\Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setURL($url, $this->getParams(array('guest_session_id' => $this->session)));
        $result = $curl->post($curl->url, array('value' => number_format($rating, 2, '.', '')));

        return $this->process($result);
    }

    public function debug()
    {
        return $this->curl->effectiveUrl;
    }

    /**
     * @param $data
     * @return mixed
     * If api return error or we have error with connection trigger error handler.
     * It must be set earlie for triggering.
     */
    protected function process($data)
    {
        /**
         * See error status code at https://www.themoviedb.org/documentation/api/status-codes
         * 1, 12, 13 - mean all ok
         **/

        $hasErrors = $this->curl->error || (!empty($data->status_code) &&
                !in_array($data->status_code, array(1, 12, 13)));

        if ($hasErrors && $this->errorHandler) {
            call_user_func($this->errorHandler, $data, $this->curl->effectiveUrl);
        }

        return $data;
    }

    protected function makeUrl($path, $placeholders = array())
    {
        return self::RES_BASE_URL . strtr($path, $placeholders);
    }

    protected function getParams($merge = [])
    {
        $default = ['api_key' => $this->apiKey];

        if ($this->lang !== 'en') {
            $default['language'] = $this->lang;
        }

        return $merge ? array_merge($default, $merge) : $default;
    }
}