<?php

class ApiWrapper extends CApplicationComponent
{
    protected $apiClient;

    public function init()
    {
        Yii::import('app.components.moviedb.MovieDbClient');
        $app = Yii::app();
        $lang = substr($app->getLanguage(), 0, 2);
        $user = $app->getComponent('user');
        $this->apiClient = new MovieDbClient($user->getState('api_key'), $user->getState('api_session'), $lang);
        $this->apiClient->setErrorHandler(function ($result = array(), $url = '') use ($app) {
            Yii::log(json_encode(array(
                'data' => $result,
                'url' => $url
            )), CLogger::LEVEL_WARNING, 'movies.api');
            $app->getComponent('user')->logout();
            $app->getRequest()->redirect($app->getUrlManager()->createUrl('site/login'));
        });
    }

    public function client()
    {
        return $this->apiClient;
    }
}