<?php


class MovieManager
{

    public static function getMovie($id)
    {
        /**
         * @var $api MovieDbClient
         */

        $model = Movie::model()->find('external_id = :id', array(':id' => $id));

        if (empty($model)) {
            $api = Yii::app()->getComponent('movieApi')->client();
            $movie = $api->getMovie($id);

            $genres = CHtml::listData($movie->genres, 'id', 'name');
            $genres = implode(', ', $genres);
            $trailer = '';

            if ($movie->trailers && $movie->trailers->youtube) {
                $trailer = reset($movie->trailers->youtube)->source;
            }

            $model = new Movie();

            if ($movie->poster_path) {
                $fileUrl = $api->getImageUrl(1000, substr($movie->poster_path, 1));
                $file = $model->generateFileName(pathinfo($fileUrl, PATHINFO_EXTENSION));
                $storePath = Yii::getPathOfAlias(Movie::UPLOAD_PATH) . '/' . $file;
                file_put_contents($storePath, fopen($fileUrl, 'r'));
                $model->poster = $file;
            }

            $model->setAttributes(array(
                'external_id' => $id,
                'title' => $movie->original_title,
                'overview' => $movie->overview,
                'release_date' => $movie->release_date,
                'runtime' => $movie->runtime,
                'genres' => $genres,
                'trailer' => $trailer
            ));

            $model->save(false);
        }

        return $model;
    }


    public static function setRating($id, $rating)
    {
        /**
         * @var $api MovieDbClient
         */

        $api = Yii::app()->getComponent('movieApi')->client();

        if ($rating >= 1 && $rating <= 10 && intval($rating * 10) % 5 === 0) {
            return $api->pushMovieRating($id, $rating);
        }

        return false;
    }

    /**
     * @param $query
     * @param $page
     * @return MDBDataProvider
     */
    public static function getProvider($query, $page)
    {
        /**
         * @var $api MovieDbClient
         */

        $api = Yii::app()->getComponent('movieApi')->client();
        $data = $api->search($query, $page);
        return new MDBDataProvider($data);
    }
}