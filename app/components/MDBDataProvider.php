<?php

class MDBDataProvider extends CArrayDataProvider
{
    const MAX_PAGES = 5;
    const PAGE_SIZE = 20;

    public function __construct($data, $config = array())
    {
        $count = min(self::MAX_PAGES * self::PAGE_SIZE, $data->total_results);
        parent::__construct($data->results, array_merge(
            $config,
            array(
                'totalItemCount' => $count,
                'pagination' => array(
                    'pageSize' => 20,
                    'itemCount' => $count
                )
            )
        ));
    }

    protected function fetchData()
    {
        return $this->rawData;
    }

    protected function calculateTotalItemCount()
    {
        return $this->totalItemCount;
    }
}