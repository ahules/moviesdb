<?php

/**
 * This is the model class for table "movies".
 *
 * The followings are the available columns in table 'movies':
 * @property integer $id
 * @property integer $external_id
 * @property string $title
 * @property string $overview
 * @property string $release_date
 * @property integer $runtime
 * @property string $genres
 * @property string $poster
 * @property string $trailer
 */
class Movie extends CActiveRecord
{
    const UPLOAD_PATH = 'webroot.storage';

    public $poster_file;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'movies';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('external_id, title, overview, release_date', 'required'),
            array('external_id, runtime', 'numerical', 'integerOnly' => true),
            array('title, poster', 'length', 'max' => 255),
            array('genres, trailer', 'safe'),
            array('poster_file', 'file', 'allowEmpty' => true, 'types' => 'jpg, png'),
            array('poster_file', 'saveFile'),
            array('id, external_id, title, genres', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'external_id' => 'External',
            'title' => 'Title',
            'overview' => 'Overview',
            'release_date' => 'Release Date',
            'runtime' => 'Runtime',
            'genres' => 'Genres',
            'poster' => 'Poster',
            'trailer' => 'Trailer',
        );
    }

    public function generateFileName($ext)
    {
        return sha1(uniqid() . time()) . '.' . $ext;
    }

    public function saveFile()
    {
        if ($this->poster_file instanceof CUploadedFile) {
            $this->eraseFile();
            $path = Yii::getPathOfAlias(self::UPLOAD_PATH) . '/';
            $file = $this->generateFileName($this->poster_file->getExtensionName());
            $this->poster = $file;
            return $this->poster_file->saveAs($path . $file);
        }

        return false;
    }

    public function eraseFile()
    {
        if (!empty($this->poster) && is_file($path = Yii::getPathOfAlias(self::UPLOAD_PATH) . '/' . $this->poster)) {
            @unlink($path);
        }
    }

    public function getPosterPath()
    {
        if ($this->poster) {
            $web = Yii::getPathOfAlias('webroot');
            $upload = Yii::getPathOfAlias(self::UPLOAD_PATH);
            return substr($upload, strlen($web)) . '/' . $this->poster;
        }

        return false;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('external_id', $this->external_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('genres', $this->genres, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    protected function beforeDelete()
    {
        $this->eraseFile();
        return parent::beforeDelete();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Movie the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
