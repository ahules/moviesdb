<?php


class AuthForm extends CFormModel
{
    public $api_key;
    private $_identity;

    public function rules()
    {
        return array(
            array('api_key', 'required'),
            array('api_key', 'length', 'min' => 32, 'max' => 32),
            array('api_key', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'api_key' => 'Your api_key',
        );
    }

    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->api_key);
            if (!$this->_identity->authenticate()) {
                $this->addError('api_key', $this->_identity->errorMessage);
            }
        }
    }

    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->api_key);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $user = Yii::app()->user;
            $user->login($this->_identity, $this->_identity->getExpire());
            $user->setState('api_key', $this->api_key);
            $user->setState('api_session', $this->_identity->getSession());
            return true;
        }

        return false;
    }
}
