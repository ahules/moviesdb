<?php

class m160523_143332_create_index_external_id_movies extends CDbMigration
{
    public function up()
    {
        $this->createIndex('idx_movies_external_id', '{{movies}}', 'external_id', true);
    }

    public function down()
    {
        $this->dropIndex('idx_movies_external_id', '{{movies}}');
    }
}