<?php

class m160523_142213_create_table_movies extends CDbMigration
{
    public function up()
    {
        $this->createTable('{{movies}}', array(
            'id' => 'pk',
            'external_id' => 'integer not null',
            'title' => 'string not null',
            'overview' => 'text not null',
            'release_date' => 'date not null',
            'runtime' => 'integer not null default 0',
            'genres' => 'text',
            'poster' => 'string',
            'trailer' => 'string',
        ));
    }

    public function down()
    {
        $this->dropTable('{{movies}}');
    }
}