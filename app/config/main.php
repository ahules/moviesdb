<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'MoviesDb',
    'language' => 'en_US',

    'timeZone' => 'Europe/Kiev',
    'preload' => array('log'),

    'aliases' => array(
        'app' => 'application'
    ),

    'import' => array(
        'application.models.*',
        'application.components.*',
    ),

    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'xxx',
            'ipFilters' => array('127.0.0.1', '::1'),
        )
    ),

    // application components
    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
        ),

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'useStrictParsing' => true,
            'rules' => array(
                '/' => 'site/index',
                '/movies' => 'movies/index',
                '/movies/<_action:(control|edit|delete|rate)>' => 'movies/<_action>',
                '/movie/<id>' => 'movies/view',
                '/login' => '/site/login',
                '/logout' => '/site/logout'
            ),
        ),

        'movieApi' => array(
            'class' => 'app.components.ApiWrapper'
        ),

        'db' => require(dirname(__FILE__) . '/database.php'),

        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'warning',
                    'categories' => 'movies.api',
                    'logFile' => 'movies.api.log'
                )
            ),
        )
    ),

    'params' => array(
        'api-key' => 'fb265244ccbf1193de8fba34f18416aa'
    ),
);
