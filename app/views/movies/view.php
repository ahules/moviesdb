<?php

/**
 * @var $movie Movie
 */


echo CHtml::link('Edit', array('edit', 'id' => $movie->id));
$rating = CHtml::textField('rating', '', array('placeholder' => 'Values must be a multiple of 0.50')) . '&nbsp;' .
    CHtml::button('Send', array(
        'id' => 'rating-btn',
        'data-external-id' => $movie->external_id
    ));

$this->widget('zii.widgets.CDetailView', array(
    'data' => $movie,
    'attributes' => array(
        'id',
        'external_id',
        'title',
        'overview',
        'release_date',
        'runtime',
        'genres',
        array('value' => $rating, 'label' => 'Rating', 'type' => 'raw'),
        array(
            'name' => 'poster',
            'type' => 'raw',
            'value' => $movie->poster ? CHtml::image($movie->getPosterPath(), '', array('style' => 'max-width: 100%;')) : 'Not set'
            ),
        array('name' => 'trailer', 'type' => 'raw', 'value' =>  $movie->trailer ? CHtml::tag('iframe',
            array(
                'width' => '640',
                'height' => '390',
                'src' => 'http://www.youtube.com/embed/' . $movie->trailer,
                'frameborder' => 0
        ), '') : 'Not set')
    )
));

$cs = Yii::app()->getComponent('clientScript');
$cs->registerCoreScript('jquery');
$cs->registerScriptFile('/js/rating.js', CClientScript::POS_END);