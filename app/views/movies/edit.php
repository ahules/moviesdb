<?php
/* @var $model Movie */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'movie-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'pure-form',
            'enctype' => 'multipart/form-data'
        )
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'external_id'); ?>
        <?php echo $form->textField($model, 'external_id', array('readonly' => true)); ?>
        <?php echo $form->error($model, 'external_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'overview'); ?>
        <?php echo $form->textArea($model, 'overview', array('rows' => 12, 'cols' => 80)); ?>
        <?php echo $form->error($model, 'overview'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'release_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'release_date',
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => 'yy-mm-dd',
            )
        )); ?>
        <?php echo $form->error($model, 'release_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'runtime'); ?>
        <?php echo $form->textField($model, 'runtime'); ?>
        <?php echo $form->error($model, 'runtime'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'genres'); ?>
        <?php echo $form->textField($model, 'genres', array('size' => 80)); ?>
        <?php echo $form->error($model, 'genres'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'poster'); ?>
        <?php if ($model->poster): ?>
            <div>
                <?= CHtml::image($model->getPosterPath(), '', array('style' => 'max-width: 200px;')); ?>
            </div>
        <?php endif;?>
        <?php echo $form->fileField($model, 'poster_file'); ?>
        <?php echo $form->error($model, 'poster_file'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'trailer'); ?>
        <?php echo $form->textField($model, 'trailer', array('size' => 80, 'placeholder' => 'Youtube video id')); ?>
        <?php echo $form->error($model, 'trailer'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save', array('class' => 'pure-button pure-button-primary')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->