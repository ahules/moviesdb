<?php

/**
 * @var $dataProvider CDataProvider
 */
?>

<ul>
    <li><?= CHtml::link('Newest movies', array('index', 'filter_newest' => 1)); ?></li>
    <li><?= CHtml::link('Default', array('index')); ?></li>
</ul>
<?php

$dateFormat = Yii::app()->getLocale()->getDateFormat('medium');
$this->widget('zii.widgets.grid.CGridView', array(
    'columns' => array(
        array(
            'name' => 'id',
            'headerHtmlOptions' => array('width' => '100'),
            'htmlOptions' => array('class' => 'text-center')
        ),
        array('name' => 'title'),
        array(
            'headerHtmlOptions' => array('width' => '100'),
            'htmlOptions' => array('class' => 'text-center'),
            'name' => 'release_date',
            'value' => sprintf('Yii::app()->getDateFormatter()->format("%s", $data->release_date)', $dateFormat)
        ),
        array(
            'class' => 'CLinkColumn',
            'htmlOptions' => array('class' => 'text-center'),
            'label' => 'View',
            'urlExpression' => 'array("movies/view", "id" => $data->id)'
        )
    ),
    'dataProvider' => $dataProvider,
    'ajaxUpdate' => false
));




