<?php

/**
 * @var $dataProvider CDataProvider
 */

$dateFormat = Yii::app()->getLocale()->getDateFormat('medium');
$this->widget('zii.widgets.grid.CGridView', array(
    'columns' => array(
        array(
            'name' => 'id',
            'headerHtmlOptions' => array('width' => '100'),
            'htmlOptions' => array('class' => 'text-center')
        ),
        array('name' => 'title'),
        array(
            'headerHtmlOptions' => array('width' => '100'),
            'htmlOptions' => array('class' => 'text-center'),
            'name' => 'release_date',
            'value' => sprintf('Yii::app()->getDateFormatter()->format("%s", $data->release_date)', $dateFormat)
        ),
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('class' => 'text-center'),
            'template' => '{update} {delete}',
            'updateButtonUrl' => 'Yii::app()->controller->createUrl("edit",array("id"=>$data->primaryKey))'
        )
    ),
    'dataProvider' => $dataProvider,
    'ajaxUpdate' => false
));