<?php
/* @var $this SiteController */
/* @var $model AuthForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Auth';
$this->breadcrumbs = array(
    'Auth',
);
?>

<div class="form auth">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('class' => 'pure-form ')
    )); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'api_key'); ?>
        <?php echo $form->textField($model, 'api_key', array('maxlength' => 32)); ?>
        <?php echo $form->error($model, 'api_key'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit', array('class' => 'pure-button pure-button-primary pull-right')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>

