<?php


/**
 * @todo: set page titles and breadcrumbs
 */

class MoviesController extends FrontController
{
    public function filters()
    {
        return array(
            'postOnly + delete rate',
            'accessControl',
        );
    }

    public function actionIndex($page = 1)
    {
        if (Yii::app()->getRequest()->getRequestUri() === '/') {
            $this->redirect(['movies/index']);
        }

        $query = array('sort_by' => 'popularity.desc');

        if (Yii::app()->getRequest()->getParam('filter_newest')) {
            /**
             * There we must define period [from and to],
             * if we not define date to, you can see films that will
             * showing at 2020y.
             */
            $query['release_date.gte'] = (new DateTime())->sub(new DateInterval('P2M'))->format('Y-m-d');
            $query['release_date.lte'] = (new DateTime())->add(new DateInterval('P1W'))->format('Y-m-d');
            $query['sort_by'] = 'release_date.desc';
        }

        $provider = MovieManager::getProvider($query, $page);

        $this->render('index', array(
            'dataProvider' => $provider
        ));
    }

    public function actionView($id)
    {
        $movie = MovieManager::getMovie($id);
        $this->render('view', array(
            'movie' => $movie
        ));
    }

    public function actionEdit($id)
    {
        $model = Movie::model()->findByPk(intval($id));

        if (isset($_POST['Movie'])) {
            $model->attributes = $_POST['Movie'];
            $model->poster_file = CUploadedFile::getInstance($model, 'poster_file');
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->external_id));
            }
        }

        $this->render('edit', array('model' => $model));
    }

    public function actionDelete()
    {
        $model = $this->loadModel(Yii::app()->getRequest()->getParam('id'));
        $model->delete();
        $this->redirect(array('control'));
    }
    
    public function actionControl()
    {
        $dataProvider = Movie::model()->search();
        $this->render('control', array(
            'dataProvider' => $dataProvider
        ));
    }

    public function actionRate()
    {
        $this->layout = false;
        $id = Yii::app()->getRequest()->getPost('id');
        $rating = Yii::app()->getRequest()->getPost('rating');

        if (MovieManager::setRating($id, $rating)) {
            $this->renderJSON(array('status' => 'ok'));
        } else {
            throw new CHttpException(400);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Movie the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Movie::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}