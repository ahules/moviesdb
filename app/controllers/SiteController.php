<?php

class SiteController extends FrontController
{

    public function accessRules()
    {
        return array(
            array('allow', 'actions' => array('login', 'error', 'index')),
            array('allow', 'actions' => array('logout'), 'users' => array('@')),
            array('deny')
        );
    }

    public function actionIndex()
    {
        $this->redirect(['movies/index']);
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Logs in  user and redirect to returnUrl.
     */
    public function actionLogin()
    {
        $model = new AuthForm();
        $model->api_key = Yii::app()->params['api-key'];

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'auth-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['AuthForm'])) {
            $model->attributes = $_POST['AuthForm'];
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }

        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}